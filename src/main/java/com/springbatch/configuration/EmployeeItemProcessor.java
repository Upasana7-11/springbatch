package com.springbatch.configuration;

import org.springframework.batch.item.ItemProcessor;

import com.springbatch.model.Employee;

public class EmployeeItemProcessor implements ItemProcessor<Employee, Employee> {

	@Override
	public Employee process(Employee emp) throws Exception {
		return emp;
	}

}
